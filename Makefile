SRC_DIR := src
OBJ_DIR := build/obj
BIN_DIR := build/bin

EXE := $(BIN_DIR)/helloworld
SRC := $(wildcard $(SRC_DIR)/*.cpp)
OBJ := $(SRC:$(SRC_DIR)/%.cpp=$(OBJ_DIR)/%.o)

CC=gcc
CPPFLAGS := -Iinclude -MMD -MP
CFLAGS   := -Wall `pkg-config --cflags gtk+-3.0`
LDFLAGS  := -Llib `pkg-config --libs gtk+-3.0`
LDLIBS   := -lm

.PHONY: all clean

all: $(EXE)

$(EXE): $(OBJ) | $(BIN_DIR)
	$(CC) $^ $(LDLIBS) -o $@ $(LDFLAGS) 

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.cpp | $(OBJ_DIR)
	$(CC) $(CPPFLAGS) $(CFLAGS) -c $< -o $@

$(BIN_DIR) $(OBJ_DIR):
	mkdir -p $@

clean:
	@$(RM) -rv $(BIN_DIR) $(OBJ_DIR)

-include $(OBJ:.o=.d)