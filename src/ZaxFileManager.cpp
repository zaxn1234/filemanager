#include <gtk/gtk.h>
#include <gtk/gtkapplication.h>
#include "ZaxFileManager.h"
#include "GUI.h"

const gchar *appName = "tk.izaakw.cpp.filemanager";

/**
 * Callback for button when clicked
 */
void onClick(GtkButton *button, gpointer data)
{
    // Change button label
    //gtk_button_set_label(button, "Clicked!!!");
}

int main(int argc, char *argv[]) 
{
    // Create application
    GtkApplication *app = gtk_application_new(appName, G_APPLICATION_FLAGS_NONE);

    // Set up callback when app activated
    g_signal_connect(app, "activate", G_CALLBACK(inits), NULL);

    // Run app and return status
    int status = g_application_run(G_APPLICATION(app), argc, argv);

    // Dereference app (free it)
    g_object_unref(app);

    // Return app run status
    return status;
}