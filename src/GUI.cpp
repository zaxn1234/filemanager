#include <gtk/gtk.h>
#include <gtk/gtkapplication.h>
#include "ZaxFileManager.h"
#include "GUI.h"

const gchar *appTitle = "FileManager";

/**
 * Initialise things we need in the app
 *   such as windows/button/widgets etc
 */
void inits(GApplication *app, gpointer data)
{
    // Create the window and set it to 800x600
    GtkWidget *window = gtk_application_window_new(GTK_APPLICATION(app));
    gtk_window_set_default_size(GTK_WINDOW(window), 800, 600);
    gtk_window_set_title(GTK_WINDOW(window), appTitle);


    // Create a button widget
    //GtkWidget *button = gtk_button_new_with_label("Click me!");
    //g_signal_connect(button, "clicked", G_CALLBACK(onClick), window);
    
    // Add button to window
    //gtk_container_add(GTK_CONTAINER(window), button);

    // Menu
    GtkWidget *menu = gtk_menu_new();
    GtkWidget *menuItem = gtk_menu_item_new_with_mnemonic("_MenuItem");
    GtkWidget *subMenu = gtk_menu_new();
    GtkWidget *menuQuit = gtk_menu_item_new_with_label("Quit");

    gtk_menu_shell_append(GTK_MENU_SHELL (subMenu), menuQuit);
    gtk_menu_item_set_submenu(GTK_MENU_ITEM (menuItem), subMenu);
    gtk_menu_shell_append(GTK_MENU_SHELL (menu), menuItem);

    gtk_application_window_set_show_menubar(GTK_APPLICATION_WINDOW(window), true);

    // Show window (and child widgets)
    gtk_widget_show_all(GTK_WIDGET(window));


    // FOR GLADE BUILDER FILE:

    // Create builder and load interface */
    // builder = gtk_builder_new();
    // gtk_builder_add_from_file( builder, "dialog.builder", NULL );

    // /* Obtain widgets that we need */
    // window = GTK_WIDGET( gtk_builder_get_object( builder, "window1" ) );
    // data.quit = GTK_WIDGET( gtk_builder_get_object( builder, "dialog1" ) );
    // data.about = GTK_WIDGET( gtk_builder_get_object( builder, "aboutdialog1" ) );

    // /* Connect callbacks */
    // gtk_builder_connect_signals( builder, &data );

    // /* Destroy builder */
    // g_object_unref( G_OBJECT( builder ) );


}